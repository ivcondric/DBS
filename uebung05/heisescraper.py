#!/usr/bin/env python3
from bs4 import BeautifulSoup
import requests, re

# this function returns a soup page object
def getPage(url):
    r = requests.get(url)
    data = r.text
    spobj = BeautifulSoup(data, "lxml")
    return spobj

# scraper website: https://www.heise.de/thema/https
def main():
    heise_https_url = "https://www.heise.de/thema/https"

    # waehle div Element mit den Ueberschriften aus
    content = getPage(heise_https_url)
    content = content.find("div", { "class" : "keywordliste" })
    content = content.findAll("div", { "class" : "recommendation" })
    headings, keywords, keywordsDict = [], [], {}

    print("--- Alle Ueberschriften ---")
    # speichere alle Ueberschriften
    for c in content:
        heading = c.find("header").text.split("\n")[0]
        print(heading)
        headings.append(heading)
    print()

    # speichere das Vorkommen der einzelnen Woerter in den Ueberschriften
    for h in headings:
        l = re.findall("\w+", h)
        for word in l:
            w = word.lower()
            if l in keywords:
                keywordsDict[w] += 1
            else:
                keywordsDict[w] = 1

    for k in keywordsDict.keys():
        keywords.append((k, keywordsDict[k]))

    # sortiere nach Wort
    keywords = sorted(keywords, key=lambda x : x [0])
    # sortiere nach Vorkommen (absteigend)
    keywords = sorted(keywords, key=lambda x : x [1], reverse=True)

    # gebe erste drei Woerter aus
    print("--- Top 3 Woerter ---")
    for i in range(3):
        print(keywords[i][1], ": " + keywords[i][0] )

if __name__ == '__main__':
    main()
