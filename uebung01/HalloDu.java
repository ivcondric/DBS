import java.util.*;

class HalloDu {
    // Attribut Name
    String name;

    // Konstruktor
    public HalloDu() {
        System.out.println("Wie ist dein Name?");
        Scanner scan = new Scanner(System.in);
        name = scan.nextLine();
    }

    // String-Darstellung des Objektes für println usw.
    public String toString() {
        return "Hallo " + this.name + "!";
    }

    // Erstelle neues HalloDu Objekt und gebe dieses aus
    public static void main(String[] args) {
        HalloDu h = new HalloDu();
        System.out.println(h);
    }
}
