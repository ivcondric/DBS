#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import psycopg2, requests, json, csv, cStringIO, codecs

# Es werden die tweets aus dem stream in die Datenbank eingefuegt.
# Dabei werden nur tweetid, timestamp, text und username beachtet.
# Die anderen Daten sind fuer das Speichern des Tweets nicht
# relevant. Es werden nur Tweets gespeichert, die keine retweets sind,
# damit keine Tweets doppelt vorkommen.

class StdOutListener(StreamListener):
    # A listener handles tweets that are received from the stream.
    # This is a basic listener that just prints received tweets to stdout.
    def __init__(self, max, db):
        # listener akzeptiert nur max eingaben
        self.count = 0
        self.max = max
        self.db = db

    def on_data(self, data):
        if (self.count < self.max):
            tweet = json.loads(data)
            print("@" + tweet['user']['screen_name'])
            print(tweet['text'])
            if not tweet['retweeted']:
                self.db.insertTweet(
                    tweet['id'], tweet['timestamp_ms'][:-3], tweet['text'], tweet['user']['screen_name']
                )
                self.count += 1
            return True

        else:
            return False

    def on_error(self, status):
        print(status)

# Klasse uebernommen von https://docs.python.org/2/library/csv.html
# damit koennen auch unicode Zeichen geschrieben werden
class UnicodeWriter():
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """
    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

# datenbankklasse fuer PostgreSQL
class TrumpDatabase():
    def __init__(self):
        conn = None
        cur = None
        csvfile = None
        writer = None

        try:
            # connect to the PostgreSQL server
            conn = psycopg2.connect(host="localhost", database="dbs", user="testuser", password="testpass")
            # create a cursor
            cur = conn.cursor()
        except (Exception, psycopg2.DatabaseError) as error:
            pass

        try:
            csvfile = open('twitter.csv', 'wb')
            writer = UnicodeWriter(csvfile)
        except:
            pass

        self.conn = conn
        self.cur = cur
        self.writer = writer
        self.csvfile = csvfile

    def insertTweet(self, id, time, text, author):
        # fuege tweet in datenbank ein
        if self.conn != None:
            self.cur.execute('INSERT INTO tweet VALUES (%s, %s, %s, %s)',
                (id, time, text, author,)
            )
        # fuege tweet zur csv hinzu
        if self.writer != None:
            self.writer.writerow((str(id), time, text, author))

    def close(self):
        # schliesse datenbankverbindung und csv datei
        if self.conn != None:
            self.conn.commit()
            self.cur.close()
            self.conn.close()
        if self.writer != None:
            self.csvfile.close()

def main():
    consumer_key        = "--consumer_key--"
    consumer_secret     = "--consumer_secret--"
    access_token        = "--access_token--"
    access_token_secret = "--access_token_secret--"

    db = TrumpDatabase()
    listener = StdOutListener(50, db)
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, listener)
    stream.filter(track=['@realDonaldTrump'])
    db.close()

if __name__ == '__main__':
    main()
