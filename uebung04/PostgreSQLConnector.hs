import Control.Exception
import Database.HDBC
import Database.HDBC.PostgreSQL (connectPostgreSQL)
-- Auf ubuntu heisst das zu installierende Paket libghc-hdbc-postgresql-dev

-- Quellen: https://wiki.haskell.org/HDBC-Example

main = do
    -- connection
    c <- connectPostgreSQL "host=localhost dbname=dbs user=testuser password=testpass"
    -- query
    query <- prepare c "SELECT datname FROM pg_database;"
    execute query []
    -- print result
    result <- fetchAllRows query
    putStrLn $ show result
    -- commit changes and close connection
    commit c
    disconnect c
    return ()
