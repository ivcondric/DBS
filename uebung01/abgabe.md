# Datenbanksysteme - 1. Übung

Studenten: Ivan Condric und Raphael Taxis <br>
Tutorin: Nina Kubiessa <br>
Tutorium: Mittwoch, 12-14 Uhr <br>

## 1. Aufgabe
- Datenbanksystem
  - Datenbanksystem (DB): Kombination aus einer Datenbank und dem dazu gehörigen Datenbankmanagementsystem
  - Datenbank: Sammlung von zusammenhängenden Daten, dauerhaft aktives System
  - Datenbankmanagementsystem (DBMS): Sammlung von Programmen die auf die Datenbank lesend und schreibend zugreifen
  - Datenbank und dazu passendes DBMS bilden Datenbanksystem
- Datenmodell ist eine Sprache, bestehend aus:
  - Data Defining Language (DDL): Sprache um das Datenbankschema zu beschreiben
  - Data Manipulation Language (DML): Sprache um die Daten in der Datenbank (nicht die Metadaten) zu bearbeiten und auszulesen
- physische Datenunabhängikeit
  - Änderung der internen Speicherstruktur beinflusst nicht die logische Struktur der Daten, d.h. das Schema der Datenbank wird nicht beeinflusst. Das Datenmodell bleibt also gültig
  - plattformunabhängig
- logische Datenunabhängikeit
  - Änderung der logischen Struktur, also Änderung des Schemas beinflusst nicht die Gültigkeit der Anwendungen, die auf die Datenbank zugreifen
- funktionale Anforderungen
  - Modelierung eines Geldtransfers
  - Verwaltung der Mitarbeiter in einer Firma
- nicht funktionale Anforderungen
  - Datensicherheit
  - Datenkonsistenz
- Metadaten
  - Daten über Daten, zum Beispiel: Schema einer Datenbank, Anzahl an Zeilen pro Tabelle, Speichergröße einer Tabelle


## 2. Aufgabe
- Datenbanksysteme
  - hierarchisch: Datenobjekte stehen in Eltern-Kind-Relation zueinander, jeder Datensatz hat genau einen Vorgänger, abgesehen von der Wurzel, Bsp: XML, IMS/DB
  - netzwerkartig: Datensätze stehen untereinander in einem Netzwerk in Beziehung
  - relational: Datensätze in Tabellen, verschiedene Arten von Datensätzen können in Beziehung stehen, Relationen werden auch in Tabellen dargestellt, Bsp: MySQL, PostgreSQL
  - objektorientiert: Datensätze werden wie Objekte nach dem Paradigma der Objektorientierten Programmierung verstanden, mit Konzepten wie Kapselung und Vererbung
  - dokumentorientiert: Datenbank enthält Dokument in verschiedenen Formaten ohne vorgeschriebene Struktur, Bsp: Lotus Notes

- NoSQL
  - not only SQL
  - keine relationalen Datenbanken, keine feste Tabellenschemata,
  Ziel: Leistungsprobleme vom SQL-Datenbanken entgegentreten, Vermeidung von Joins. Horizontale Skalierung, d.h. Leistungssteigerung durch Hinzufügung von Hardware
  - Dokumentorientierte Datenbank passen am ehesten in die Kategorie, weil sie keine feste Struktur und keine zwingenden Relationen zwischen den Datensätzen haben.

- RDF:
  - Resource Description Framework
  - es dient zur Abfrage von Daten, die eine Webresource beschreiben, zum Beispiel Abfrage des Autors einer Seite
  - Die Grundstruktur ist ein RDF-Tripel. Dieses besteht aus Subjekt, Prädikat und Objekt. Diese werden in der Regel durch ein sogenannten IRI, einen Internationalized Resource Identifier dargestellt. Im Falle von Subjekt und Objekt ist dies eine URL. Das Objekt kann auch ein atomischer Wert sein, zum Beispiel ein String.
  - Beispiel für ein Tripel:
    (<http://www.mi.fu-berlin.de/>, <http://purl.org/dc/elements/1.1/title>, "Fachbereich Mathematik und Informatik: Startseite"), d.h. Die Seite bzw. Resource http://www.mi.fu-berlin.de/ hat den Titel "Fachbereich Mathematik und Informatik: Startseite". Für die Prädikate gibt es verschiedene Konventionen.
  - Eine Menge von RDF-Tripeln bildet einen RDF-Graphaen. Eine Sammlung von RDF-Graphen ist ein RDF-Datensatz.

RDF Beispiel Datensatz:

| Tupel |
|-----|
|(<http://www.mi.fu-berlin.de/>, <http://purl.org/dc/elements/1.1/title>, "Fachbereich Mathematik und Informatik: Startseite")|
|(<http://www.mi.fu-berlin.de/>, <http://purl.org/dc/elements/1.1/language>, "de-DE")|
 |(<http://www.fu-berlin.de/en/index.html>, <http://purl.org/dc/elements/1.1/title>, "Freie Universität Berlin: Homepage")|
 |(<http://www.fu-berlin.de/en/index.html>, <http://purl.org/dc/elements/1.1/language>, "en")|


SPARQL Anfrage an den Beispieldatensatz:


    @prefix dc: <http://purl.org/dc/elements/1.1/> .
    SELECT ?title ?language
    WHERE
    {
      dc:title <http://www.fu-berlin.de/en/index.html>  ?title .
      dc:language  <http://www.fu-berlin.de/en/index.html> ?language
    }    


  - Die Anfrage verwendet die Definitionen für Prädikat, wie sie bei purl.org definiert sind.
  - Das Ergebnis wird in der Spalte mit dem Namen titel bzw. language zurückgegeben.
  - Es wird von der Resource http://www.fu-berlin.de/en/index.html abgefragt.

Ergebnis der SPARQL Anfrage:

| title | language |
|-------|------|
| Freie Universität Berlin: Homepage | en |

- Quellen:

    https://de.wikipedia.org/wiki/Datenbank#Verschiedene_Formen_von_Datenbanksystemen
    https://de.wikipedia.org/wiki/Hierarchisches_Datenbankmodell
    https://de.wikipedia.org/wiki/Netzwerkdatenbankmodell
    https://de.wikipedia.org/wiki/Relationale_Datenbank
    https://de.wikipedia.org/wiki/Objektdatenbank
    https://de.wikipedia.org/wiki/Dokumentenorientierte_Datenbank
    https://de.wikipedia.org/wiki/NoSQL
    https://de.wikipedia.org/wiki/Skalierbarkeit#Horizontale_Skalierung_.28scale_out.29
    https://www.w3.org/TR/rdf11-concepts/#section-triples
    https://www.w3.org/TR/rdf-sparql-query/
    https://www.obitko.com/tutorials/ontologies-semantic-web/rdf-graph-and-syntax.html
    http://xmlns.com/foaf/spec/#term_Agent


## 3. Aufgabe
- KVV: Auf welchen Seiten/Modulen ist Student XY Mitglied?
- Online-Mediathek: Was sind die 20 neusten Videos?
  Welches Video wurde im letzten Monat am meisten angeschaut?
- Photo-Sharing-Platform: Wer hat alles den Post XY kommentiert?
  Welches Foto hat die meisten likes?
- Mikrobloging-Dienst: Wem folgt der User XY?
- sozailes Netzwerk: Wer sind die Freunde von User XY? Wie viele neue Nutzer sind Facebook im letzten Jahr beigetreten?

## 4. Aufgabe

### Haskell

```Haskell
-- starte eine nicht funktionale Prozedur
-- lese Namen ein und gebe den Antworttext aus
main = do
    putStrLn "Wie ist dein Name?"
    name <- getLine
    putStrLn ("Hallo " ++ name ++ "!")
```

<br><br>

### Python

```Python
#!/usr/bin/env python2
# -*- coding: utf-8 -*-

class HalloDu():
    # Konstruktor bzw. init-Funktion
    def __init__(self):
        print "Wie ist dein Name?"
        self.name = raw_input()
        # raw_input() gibt immer einen String zurück und ist äquivalent zu
        # input() in python3. Die input() Funktion aus python2 macht
        # automatische Typumwandlung, die hier nicht erwünscht ist.

    # String-Darstellung des Objektes für print usw.
    def __str__(self):
        return "Hallo " + self.name +  "!"

# Erstelle neues HalloDu Objekt und gebe dieses aus
def main():
    h = HalloDu()
    print h

# Rufe die main Funktion auf, wenn diese Datei direkt ausgeführt wird
if __name__ == '__main__':
    main()
```

### Java

```Java
import java.util.*;

class HalloDu {
    // Attribut Name
    String name;

    // Konstruktor
    public HalloDu() {
        System.out.println("Wie ist dein Name?");
        Scanner scan = new Scanner(System.in);
        name = scan.nextLine();
    }

    // String-Darstellung des Objektes für println usw.
    public String toString() {
        return "Hallo " + this.name + "!";
    }

    // Erstelle neues HalloDu Objekt und gebe dieses aus
    public static void main(String[] args) {
        HalloDu h = new HalloDu();
        System.out.println(h);
    }
}
```
