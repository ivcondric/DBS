/*
Die Top 3 Fluggesellschaften (gemessen an den transportierten Passagieren)
Annahme: ein transportierter Passagier ist wird pro Flug gezaehlt
*/
select count(g.Fluggesellschaft-ID) as Anzahl, g.Name
from Flug f, Fluggesellschaft g
where g.ID = f.Fluggesellschaft-ID
group by g.Name
order by Anzahl desc
limit 3;

/*
Die Top 10 Passagiere (Vorname, Nachname), die am wenigsten geflogen
sind.
*/
select p.Vorname, p.Nachname
from(
    select count(f.Passagier-ID) as Fluege, p.Vorname, p.Nachname
    from Passagier p, Flug f
    where f.Passagier-ID = p.ID
    group by p.ID
    order by Fluege asc
    limit 10
);

/*
Die Top 5 Passagiere (Vorname, Nachname), die am meisten mit den
wenigsten verschiedenen Fluggesellschaften geflogen sind.
*/
select p.Vorname, p.Nachname
from(
    select count(g.Passagier-ID) as Fluege p.Vorname, p.Nachname
    from Flug f, Passagier p
    where f.Passagier-ID = p.ID
    group by p.ID
    order by Fluege desc, f.Fluggesellschaft-ID asc
    limit 5
);
