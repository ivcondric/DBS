/*
Alle Passagiere (Vorname, Nachname), die aelter als 42 Jahre sind.
*/
select distinct Vorname, Nachname
from Passagier
where
    Alter > 42;

/*
Alle Kreditkartennummern von Passagieren, die mit der Fluggesellschaft
"Lufthansa" zwischen dem 18.01.2014 (inklusive) und dem 27.09.2014 (exklusive)
geflogen sind.
*/
select distinct p.Kreditkartennummer
from Fluggesellschaft g, Passagier p, Flug f
where
    g.ID = f.Fluggesellschaft-ID and
    f.Passagier-ID = p.ID and
    g.Name = "Lufthansa" and
    f.Datum >= "2014-01-18" and
    f.Datum < "2014-09-17";

/*
Alle  Fluggesellschaften  (Name),  die  Passagiere  an  einem  Tag  transportiert
haben, an dem die Sonne mehr als 8 Stunden geschienen hat
*/
select distinct g.Name
from Fluggesellschaft g, Flug f, Wetter w
where
    g.ID = f.Fluggesellschaft-ID and
    f.Datum = w.Datum and
    w.Sonnenscheindauer > 8;

/*
Alle Passagiere (Vorname, Nachname), die nie bei gutem Wetter (Temperatur > 20,
Regenmenge < 10,  Sonnenscheindauer > 6)  geflogen sind.
*/
select distinct p.Vorname, p.Nachname
from Passagier p
where
    p.ID not in (
        select distinct p.ID
        from Passagier p, Flug f, Wetter w
        where
            p.ID = f.Passagier-ID and
            f.Datum = w.Datum and
            w.Tenpetatur > 20 and
            w.Regenmenge < 10 and
            w.Sonnenscheindauer > 6
    );
