#!/usr/bin/env python2
# -*- coding: utf-8 -*-

class HalloDu():
    # Konstruktor bzw. init-Funktion
    def __init__(self):
        print "Wie ist dein Name?"
        self.name = raw_input()
        # raw_input() gibt immer einen String zurück und ist äquivalent zu
        # input() in python3. Die input() Funktion aus python2 macht
        # automatische Typumwandlung, die hier nicht erwünscht ist.

    # String-Darstellung des Objektes für print usw.
    def __str__(self):
        return "Hallo " + self.name +  "!"

# Erstelle neues HalloDu Objekt und gebe dieses aus
def main():
    h = HalloDu()
    print h

# Rufe die main Funktion auf, wenn diese Datei direkt ausgeführt wird
if __name__ == '__main__':
    main()
