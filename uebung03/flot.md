### 4. Aufgabe: Visualisierung mit JavaScript

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"></meta>
        <title>Apple Aktie</title>
        <!--
        Vorraussetzung:
        im gleichen Ordner befindet sich der Unterordner der flot Bibliothek.
        Befehl:
        git pull https://github.com/flot/flot.git
        -->
        <script src="flot/jquery.js"></script>
        <script src="flot/jquery.flot.js"></script>
        <script src="flot/jquery.flot.time.js"></script>
    </head>
    <body>
        <div id="linechart"></div>
        <div id="barchart"></div>
        <script>
            /* Eingabedaten */
            var csv = [
                ["2017/01/02",109.30,110.80,110.30,2330417],
                ["2017/01/03",110.11,112.09,110.68,882614],
                ["2017/01/04",110.53,111.70,111.03,449615],
                ["2017/01/05",109.40,110.73,110.00,746100],
                ["2017/01/06",109.80,112.00,111.61,700163],
                ["2017/01/09",111.60,113.10,112.46,1297576],
                ["2017/01/10",111.80,112.90,112.75,1691504],
                ["2017/01/11",111.83,113.86,112.17,1521071],
                ["2017/01/12",110.94,112.60,111.76,1073743],
                ["2017/01/13",111.60,112.50,111.76,1149538],
                ["2017/01/16",111.72,112.45,112.00,861942],
                ["2017/01/17",110.00,112.29,112.08,848050],
                ["2017/01/18",112.00,112.70,112.35,528968],
                ["2017/01/19",111.99,113.09,112.71,778900],
                ["2017/01/20",112.14,113.10,112.14,610841],
                ["2017/01/23",111.04,112.50,111.74,366744],
                ["2017/01/24",110.90,111.73,111.51,734456],
                ["2017/01/25",111.70,113.65,113.65,1221101],
                ["2017/01/26",113.20,114.38,114.00,1302214],
                ["2017/01/27",113.70,114.46,114.03,739897],
                ["2017/01/30",112.90,113.99,113.35,1268146],
                ["2017/01/31",111.78,113.50,111.91,988174]
            ];

            /* linechart */
            var hoch = [];
            var tief = [];
            var tag = [];
            var volumen = [];
            var datum = 0;




            /* erzeuge Arrays mit den Wertepaaren */
            for (i = 0; i<csv.length; i ++) {
                /* erzeuge den UNIX-Timestamp in Millisekunden */
                datum = new Date(csv[i][0]).getTime();
                hoch.push([datum, csv[i][1]]);
                tief.push([datum, csv[i][2]]);
                tag.push([datum, csv[i][3]]);
                volumen.push([datum, csv[i][4]]);
            }

            /* linechart */
            $('#linechart').css('width', '600px').css('height', '400px');
            $.plot(
                $("#linechart"),
                [ hoch, tief, tag ], /* Daten */
                {
                    xaxis: {
                        mode: "time",
                        timeformat: "%d/%m/%y",
                        min: (new Date("2017/01/02")).getTime(),
                        max: (new Date("2017/01/31")).getTime()
                    },
                    yaxis: {
                        max: 115
                    },
                    colors: ["red", "green", "yellow"]
                }
            );

            /* barchart */
            $('#barchart').css('width', '600px').css('height', '400px');
            $.plot(
                $("#barchart"),
                [ volumen ],  /* Daten */
                {
                    series: {
                        bars: {
                            show: true
                        }
                    },
                    xaxis: {
                        mode: "time",
                        timeformat: "%d/%m/%y",
                        min: (new Date("2017/01/02")).getTime(),
                        max: (new Date("2017/01/31")).getTime()
                    },
                    yaxis: {
                        max: 3000000
                    },
                }
            );
        </script>
    </body>
</html>
```
