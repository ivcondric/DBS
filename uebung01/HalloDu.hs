-- starte eine nicht funktionale Prozedur
-- lese Namen ein und gebe den Antworttext aus
main = do
    putStrLn "Wie ist dein Name?"
    name <- getLine
    putStrLn ("Hallo " ++ name ++ "!")
