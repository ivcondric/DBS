#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Quellen: http://initd.org/psycopg/docs/usage.html

import psycopg2
# Auf ubuntu heisst das zu installierende Paket python-psycopg2

def main():
    # connection
    conn = psycopg2.connect(dbname="dbs", user="testuser", password="testpass", host="localhost")
    cur = conn.cursor()
    # query
    cur.execute("SELECT datname FROM pg_database;")
    # print result
    print cur.fetchall()
    # commit changes and close connection
    conn.commit()
    cur.close()
    conn.close()

if __name__ == '__main__':
    main()
