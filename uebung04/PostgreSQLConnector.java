import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/*
Der JDBC PostgreSQL Driver muss dem Classpath hinzugefuegt werden:
Auf ubuntu heisst das zu installierende Paket libpostgresql-jdbc-java
Ausfuehrung des Codes:
java -cp /usr/share/java/postgresql.jar:. PostgreSQLConnector

Quellen:
https://docs.oracle.com/javase/tutorial/jdbc/basics/processingsqlstatements.html
*/

public class PostgreSQLConnector {
    public static void main(String[] args) {
        try {
            // connection
            Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost/dbs", "testuser", "testpass"
            );
            // query
            String query = "SELECT datname FROM pg_database;";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            // print result
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (rs.next()){
                for (int i = 1; i<=columnsNumber; i++){
                    System.out.print(rs.getString(i) + ", ");
                }
                System.out.println();
            }
            // close connection
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
