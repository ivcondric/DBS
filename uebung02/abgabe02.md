# Datenbanksysteme - 2. Übung

Studenten: Ivan Condric und Raphael Taxis <br>
Tutorin: Nina Kubiessa <br>
Tutorium: Mittwoch, 12-14 Uhr <br>

## 1. Aufgabe: Grundlagen
- Zwei Entitätstypen in der Universität wären z.B. die Studenten und die Dozenten.
- Zwei Attribute eines Studenten könnten die Matrikelnummer und der Name sein.
- Das Schlüsselattribut ist das wichtigste Attribut einer Entität. Dieses Attribut ist bei jeder Entität verschieden, wodurch jede Entität identifiziert werden kann.
- Mehrere Mitarbeiter können nur für eine Abteilung arbeiten.
- Mehrere Mitarbeiter können bei mehreren Projekten arbeiten.
- Studenten sind Übungspartner von Studenten. Das Kardinalitätsverhältnis bei diesem Beispiel ist das M:N Verhältnis


## 2. Aufgabe: ER-Modellierung
![alt text](ER-Modell_01.svg)

## 3. Aufgabe: ER-Modellierung 2
### a)
<img src="ER-Modell_02_1.svg" height="670px">

### b)
![alt text](ER-Modell_02_2.svg)

### c)
- Partielle Partizipation: Wenn eine Entitätstyp A partiell an einer Relation zu einem weiteren Entitätstyp B partizipiert, kann A auch ohne B existieren.

- Totale Partizipation: Wenn eine Entitätstyp A total an einer Relation zu einem weiteren Entitätstyp B partizipiert, dann kann A nicht ohne das Vorhandensein eines B existieren, mit dem es in Relation steht.


## 4. Aufgabe: Webserver & JavaScript

#### index.html
```HTML
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Eine Webseite</title>
    <script src="src/halloDu.js"></script>
</head>
<body>
	<label for="eingabe">
		Ihr Name:
		<input id="feld" name="eingabe"/>
	</label>
	<button id="knopf" type="button" onclick="halloDu()">
		Klick mich!
	</button>
	<div id="bereich"></div>
</body>
</html>
```

### src/halloDu.js
```JavaScript
function halloDu () {
    var feld_wert = document.getElementById("feld").value;
    if (feld_wert == "") {
        document.getElementById("bereich").innerHTML = "Hallo Unbekannter!";
    }
    else {
        document.getElementById("bereich").innerHTML = "Hallo " + feld_wert + "!";
    }
}
```
